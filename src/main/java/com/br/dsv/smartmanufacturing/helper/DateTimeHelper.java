package com.br.dsv.smartmanufacturing.helper;

public class DateTimeHelper {
	
	public static Long hourToMilisseconds(String hour){
		String[] tokens = hour.split(":");
		Long hours = Long.parseLong(tokens[0]) * 3600000;
		Long minutes = Long.parseLong(tokens[1]) * 60000;
		Long seconds = Long.parseLong(tokens[2]) * 1000;
				
		return hours + minutes + seconds;
	}

}
