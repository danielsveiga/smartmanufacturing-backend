package com.br.dsv.smartmanufacturing.controllers;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.br.dsv.smartmanufacturing.model.Person;
import com.br.dsv.smartmanufacturing.model.PersonCorporation;
import com.br.dsv.smartmanufacturing.model.PersonIndividual;
import com.br.dsv.smartmanufacturing.services.IPersonService;

@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class PersonController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);
	
	@Autowired
	private IPersonService personService;
	
	@RequestMapping(value="/person", method = RequestMethod.GET)
	@ResponseBody
	public Page<Person> find(
			@RequestParam(value="type", required=false) String type,
			@RequestParam(value="pageSize", required=false) Integer pageSize, 
			@RequestParam(value="pageNumber", required=false) Integer pageNumber){
		
		if(pageSize==null){
			pageSize=20;
		}
		
		if(pageNumber==null){
			pageNumber=0;
		}
				
		Pageable pageRequest = new PageRequest(pageNumber, pageSize);
		
		Page<Person> result = null;
		if(type!=null){
			if(type.equals("client")){
				result = personService.findByIsClient(pageRequest);
			} else if(type.equals("provider")){
				result = personService.findByIsProvider(pageRequest);
			} 
		} else {
			result = personService.findAll(pageRequest);
		}
		
		return result;
	}
	
	@RequestMapping(value="/person/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Person findById(
			@PathVariable(value="id") Long id){
		
		return personService.findById(id);
	}
	
	@Transactional
	@RequestMapping(value="/personIndividual", method = RequestMethod.POST)
	public ResponseEntity add(@RequestBody PersonIndividual person) throws Exception{
		LOGGER.info("Receiving request to create person individual");

		person = (PersonIndividual) personService.save(person);

		LOGGER.info("Person individual save as success with id " + person.getId());
		return new ResponseEntity(person, HttpStatus.CREATED);
	}
	
	@Transactional
	@RequestMapping(value="/personCorporation", method = RequestMethod.POST)
	public ResponseEntity add(@RequestBody PersonCorporation person) throws Exception{
		LOGGER.info("Receiving request to create person corporation");

		person = (PersonCorporation) personService.save(person);

		LOGGER.info("Person corporation save as success with id " + person.getId());
		return new ResponseEntity(person, HttpStatus.CREATED);
	}
	
	@Transactional
	@RequestMapping(value="/person", method = RequestMethod.PUT)
	public ResponseEntity update(@RequestBody Person person) throws Exception{
		LOGGER.info("Receiving request to update person");

		person = personService.update(person);

		LOGGER.info("Person save as success with id " + person.getId());
		return new ResponseEntity(person, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/person/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable(value="id") Long id) throws Exception{
		LOGGER.info("Receiving request to delete person");

		personService.delete(id);

		LOGGER.info("Person deleted as success with id " + id);
		return new ResponseEntity(HttpStatus.OK);
	}

}
