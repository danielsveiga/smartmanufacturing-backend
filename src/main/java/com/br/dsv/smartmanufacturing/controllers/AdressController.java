package com.br.dsv.smartmanufacturing.controllers;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.br.dsv.smartmanufacturing.model.Adress;
import com.br.dsv.smartmanufacturing.services.IAdressService;

@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class AdressController {
	
	@Autowired
	IAdressService adressService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdressController.class);
	
	@RequestMapping(value="/adress", method = RequestMethod.GET)
	@ResponseBody
	public Page<Adress> find(
			@RequestParam(value="pageSize", required=false) Integer pageSize, 
			@RequestParam(value="pageNumber", required=false) Integer pageNumber){
		
		LOGGER.info("Receiving request to get all adress");
		
		if(pageSize==null){
			pageSize=20;
		}
		
		if(pageNumber==null){
			pageNumber=0;
		}
				
		Pageable pageRequest = new PageRequest(pageNumber, pageSize);
		return adressService.findAll(pageRequest);
	}
	
	@Transactional
	@RequestMapping(value="/adress", method = RequestMethod.POST)
	public ResponseEntity add(@RequestBody Adress adress) throws Exception{
		LOGGER.info("Receiving request to create adress");

		adress = adressService.save(adress);

		LOGGER.info("Adress saved as success with id " + adress.getId());
		return new ResponseEntity(adress, HttpStatus.CREATED);
	}
	
	@Transactional
	@RequestMapping(value="/adress", method = RequestMethod.PUT)
	public ResponseEntity update(@RequestBody Adress adress) throws Exception{
		LOGGER.info("Receiving request to update client");

		adress = adressService.update(adress);

		LOGGER.info("Client save as success with id " + adress.getId());
		return new ResponseEntity(adress, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/adress/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable(value="id") Long id) throws Exception{
		LOGGER.info("Receiving request to delete adress");

		adressService.delete(id);

		LOGGER.info("Adress deleted as success with id " + id);
		return new ResponseEntity(HttpStatus.OK);
	}

}
