package com.br.dsv.smartmanufacturing.controllers;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.br.dsv.smartmanufacturing.helper.DateTimeHelper;
import com.br.dsv.smartmanufacturing.model.Machine;
import com.br.dsv.smartmanufacturing.services.IMachineService;

@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class MachineController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MachineController.class);
	
	@Autowired
	private IMachineService machineService;
	
	@RequestMapping(value="/machine", method = RequestMethod.GET)
	@ResponseBody
	public Page<Machine> find(
			@RequestParam(value="pageSize", required=false) Integer pageSize, 
			@RequestParam(value="pageNumber", required=false) Integer pageNumber){
		
		if(pageSize==null){
			pageSize=20;
		}
		
		if(pageNumber==null){
			pageNumber=0;
		}
				
		Pageable pageRequest = new PageRequest(pageNumber, pageSize);
		return machineService.findAll(pageRequest);
	}
	
	@RequestMapping(value="/machine/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Machine findById(
			@PathVariable(value="id") Long id){
		
		DateTimeHelper.hourToMilisseconds("1:10:00");
		return machineService.findById(id);
	}
	
	@Transactional
	@RequestMapping(value="/machine", method = RequestMethod.POST)
	public ResponseEntity add(@RequestBody Machine machine) throws Exception{
		LOGGER.info("Receiving request to create machine");

		machine = machineService.save(machine);

		LOGGER.info("Machine save as success with id " + machine.getId());
		return new ResponseEntity(machine, HttpStatus.CREATED);
	}
	
	@Transactional
	@RequestMapping(value="/machine", method = RequestMethod.PUT)
	public ResponseEntity update(@RequestBody Machine machine) throws Exception{
		LOGGER.info("Receiving request to update person");

		machine = machineService.update(machine);

		LOGGER.info("Machine save as success with id " + machine.getId());
		return new ResponseEntity(machine, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/machine/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable(value="id") Long id) throws Exception{
		LOGGER.info("Receiving request to delete machine");

		machineService.delete(id);

		LOGGER.info("Machine deleted as success with id " + id);
		return new ResponseEntity(HttpStatus.OK);
	}


}
