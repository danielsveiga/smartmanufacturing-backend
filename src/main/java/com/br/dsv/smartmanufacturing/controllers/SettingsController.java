package com.br.dsv.smartmanufacturing.controllers;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.br.dsv.smartmanufacturing.model.Settings;
import com.br.dsv.smartmanufacturing.services.ISettingsService;

@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class SettingsController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(SettingsController.class);
	
	@Autowired
	private ISettingsService settingsService;
	
	@RequestMapping(value="/settings", method = RequestMethod.GET)
	@ResponseBody
	public Page<Settings> find(
			@RequestParam(value="pageSize", required=false) Integer pageSize, 
			@RequestParam(value="pageNumber", required=false) Integer pageNumber){
		
		if(pageSize==null){
			pageSize=20;
		}
		
		if(pageNumber==null){
			pageNumber=0;
		}
				
		Pageable pageRequest = new PageRequest(pageNumber, pageSize);
		Page<Settings> result = settingsService.findAll(pageRequest);
		return result;
	}
	
	@RequestMapping(value="/settings/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Settings findById(
			@PathVariable(value="id") Long id){
		
		return settingsService.findById(id);
	}
	
	@Transactional
	@RequestMapping(value="/settings", method = RequestMethod.POST)
	public ResponseEntity add(@RequestBody Settings settings) throws Exception{
		LOGGER.info("Receiving request to create settings");

		settings = settingsService.save(settings);

		LOGGER.info("Settings corporation save as success with id " + settings.getId());
		return new ResponseEntity(settings, HttpStatus.CREATED);
	}
	
	@Transactional
	@RequestMapping(value="/settings", method = RequestMethod.PUT)
	public ResponseEntity update(@RequestBody Settings settings) throws Exception{
		LOGGER.info("Receiving request to update settings");

		settings = settingsService.update(settings);

		LOGGER.info("Settings save as success with id " + settings.getId());
		return new ResponseEntity(settings, HttpStatus.CREATED);
	}
}
