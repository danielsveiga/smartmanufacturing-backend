package com.br.dsv.smartmanufacturing.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.br.dsv.smartmanufacturing.model.Machine;
import com.br.dsv.smartmanufacturing.repository.MachineRepository;
import com.br.dsv.smartmanufacturing.services.IMachineService;

@Service
public class MachineServiceImpl implements IMachineService{
	
	@Autowired
	MachineRepository machineRepository;

	public Page<Machine> findAll(Pageable pageRequest) {
		return machineRepository.findAll(pageRequest);
	}

	public Machine findById(Long id) {
		return machineRepository.findById(id);
	}

	public Machine update(Machine machine) {
		return machineRepository.save(machine);
	}

	public Machine save(Machine machine) {
		return machineRepository.save(machine);
	}

	public void delete(Long id) {
		machineRepository.delete(id);
		
	}

}
