package com.br.dsv.smartmanufacturing.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.br.dsv.smartmanufacturing.model.Person;
import com.br.dsv.smartmanufacturing.repository.AdressRepository;
import com.br.dsv.smartmanufacturing.repository.PersonRepository;
import com.br.dsv.smartmanufacturing.services.IPersonService;

@Service
public class PersonServiceImpl implements IPersonService{
	
	@Autowired
	PersonRepository personRepository;
	
	@Autowired
	AdressRepository adressRepository;

	public Page<Person> findAll(Pageable pageRequest) {
		return personRepository.findAll(pageRequest);
	}

	public Person findById(Long id) {
		return personRepository.findById(id);
	}

	public Person save(Person person) {
		adressRepository.save(person.getAdress());
		return personRepository.save(person);
	}

	public Person update(Person person) {
		return personRepository.save(person);
	}

	public void delete(Long id) {
		personRepository.delete(id);
	}

	public Page<Person> findByIsClient(Pageable pageResquest) {
		return personRepository.findByIsClient(pageResquest);
	}
	
	public Page<Person> findByIsProvider(Pageable pageResquest) {
		return personRepository.findByIsProvider(pageResquest);
	}

}
