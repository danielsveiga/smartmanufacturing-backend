package com.br.dsv.smartmanufacturing.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.br.dsv.smartmanufacturing.model.Settings;

public interface ISettingsService {
	
	public Page<Settings> findAll(Pageable pageRequest);
	
	public Settings findById(Long id);
	
	public Settings save(Settings settings);
	
	public Settings update(Settings settigs);

}
