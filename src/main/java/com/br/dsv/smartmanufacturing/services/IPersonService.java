package com.br.dsv.smartmanufacturing.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.br.dsv.smartmanufacturing.model.Person;

public interface IPersonService {
	
	public Page<Person> findAll(Pageable pageRequest);
	
	public Person findById(Long id);
	
	public Page<Person> findByIsClient(Pageable pageResquest);
	
	public Page<Person> findByIsProvider(Pageable pageResquest);
	
	public Person save(Person person);
	
	public Person update(Person person);
	
	public void delete(Long id);

}
