package com.br.dsv.smartmanufacturing.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.br.dsv.smartmanufacturing.model.Adress;
import com.br.dsv.smartmanufacturing.repository.AdressRepository;
import com.br.dsv.smartmanufacturing.services.IAdressService;

@Service
public class AdressServiceImpl implements IAdressService{
	
	@Autowired
	AdressRepository adressRepository;

	public Page<Adress> findAll(Pageable pageRequest) {
		return adressRepository.findAll(pageRequest);
	}

	public Adress save(Adress adress) {
		return adressRepository.save(adress);
	}
	
	public Adress update(Adress adress){
		return adressRepository.save(adress);
	}
	
	public void delete(Long id){
		adressRepository.delete(id);
	}

}
