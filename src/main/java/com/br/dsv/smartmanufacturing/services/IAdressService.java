package com.br.dsv.smartmanufacturing.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.br.dsv.smartmanufacturing.model.Adress;

public interface IAdressService {
	
	public Page<Adress> findAll(Pageable pageRequest);
	
	public Adress save(Adress adress);
	
	public Adress update(Adress adress);
	
	public void delete(Long id);

}
