package com.br.dsv.smartmanufacturing.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.br.dsv.smartmanufacturing.model.Machine;

public interface IMachineService{
	
	public Page<Machine> findAll(Pageable pageRequest);
	
	public Machine findById(Long id);
	
	public Machine update(Machine machine);
	
	public Machine save(Machine machine);
	
	public void delete(Long id);

}
