package com.br.dsv.smartmanufacturing.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.br.dsv.smartmanufacturing.model.Settings;
import com.br.dsv.smartmanufacturing.repository.SettingsRepository;
import com.br.dsv.smartmanufacturing.services.ISettingsService;

@Service
public class SettingsServiceImpl implements ISettingsService{

	@Autowired
	SettingsRepository settingsRepository;

	public Page<Settings> findAll(Pageable pageRequest) {
		return settingsRepository.findAll(pageRequest);
	}

	public Settings findById(Long id) {
		return settingsRepository.findById(id);
	}

	public Settings save(Settings settings) {
		return settingsRepository.save(settings);
	}

	public Settings update(Settings settings) {
		return settingsRepository.save(settings);
	}

}
