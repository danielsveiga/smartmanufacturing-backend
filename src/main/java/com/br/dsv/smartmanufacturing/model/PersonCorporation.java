package com.br.dsv.smartmanufacturing.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("personcorporation")
public class PersonCorporation extends Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3673922613729586463L;

	private String cnpj;
	
	private String businessName;
	
	private String ie;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}
}
