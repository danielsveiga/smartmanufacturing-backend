package com.br.dsv.smartmanufacturing.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="budget_items")
public class BudgetItems implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6878247611189437574L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="provider_id", referencedColumnName="id")
	private Person provider;
	
	@ManyToOne
	@JoinColumn(name="material_id", referencedColumnName="id")
	private Material material;
	
	private String dimensions;
	
	private Float weightPart;
	
	private Double kgPrice;
	
	private Float tax;
	
	private Double totalPrice;
	
	private Integer qty;
	
	private Double partPrice;
	
	private Boolean isRawMaterial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Person getProvider() {
		return provider;
	}

	public void setProvider(Person provider) {
		this.provider = provider;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public String getDimensions() {
		return dimensions;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public Float getWeightPart() {
		return weightPart;
	}

	public void setWeightPart(Float weightPart) {
		this.weightPart = weightPart;
	}

	public Double getKgPrice() {
		return kgPrice;
	}

	public void setKgPrice(Double kgPrice) {
		this.kgPrice = kgPrice;
	}

	public Float getTax() {
		return tax;
	}

	public void setTax(Float tax) {
		this.tax = tax;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Double getPartPrice() {
		return partPrice;
	}

	public void setPartPrice(Double partPrice) {
		this.partPrice = partPrice;
	}

	public Boolean getIsRawMaterial() {
		return isRawMaterial;
	}

	public void setIsRawMaterial(Boolean isRawMaterial) {
		this.isRawMaterial = isRawMaterial;
	}
	
}
