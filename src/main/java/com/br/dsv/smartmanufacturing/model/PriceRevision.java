package com.br.dsv.smartmanufacturing.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="price_revision")
public class PriceRevision implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -823379986830385099L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
