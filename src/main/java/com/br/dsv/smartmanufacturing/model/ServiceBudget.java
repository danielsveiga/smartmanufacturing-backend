package com.br.dsv.smartmanufacturing.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="service_budget")
public class ServiceBudget implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1029709318397537724L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date date;
	
	private String drawingNumber;
	
	private String drawingNumberRevision;
	
	private String createdBy;
	
	private String approvedBy;
	
	private PriceRevision priceRevision;
	
	private String budgetNumber;
	
	private String description;
	
	private Integer qty;
	
	private Double otherServices;
	
	private Double laborPrice;
	
	private Double rawMaterialPrice;
	
	private Double finalPrice;
	
	private String freightType;
	
	private Date deliveryDeadline;
	
	private Date paymentDeadline;
	
	private Tax tax;
	
	private Date expirationDate;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<BudgetItems> budgetItems;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Person client;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name="service_budget_machines",
		    joinColumns=@JoinColumn(name="service_budget_id", referencedColumnName="id"),
		    inverseJoinColumns=@JoinColumn(name="machine_id", referencedColumnName="id"))
	private List<Machine> machines;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBudgetNumber() {
		return budgetNumber;
	}

	public void setBudgetNumber(String budgetNumber) {
		this.budgetNumber = budgetNumber;
	}

	public List<Machine> getMachines() {
		return machines;
	}

	public void setMachines(List<Machine> machines) {
		this.machines = machines;
	}

	public PriceRevision getPriceRevision() {
		return priceRevision;
	}

	public void setPriceRevision(PriceRevision priceRevision) {
		this.priceRevision = priceRevision;
	}

	public Person getClient() {
		return client;
	}

	public void setClient(Person client) {
		this.client = client;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDrawingNumber() {
		return drawingNumber;
	}

	public void setDrawingNumber(String drawingNumber) {
		this.drawingNumber = drawingNumber;
	}

	public String getDrawingNumberRevision() {
		return drawingNumberRevision;
	}

	public void setDrawingNumberRevision(String drawingNumberRevision) {
		this.drawingNumberRevision = drawingNumberRevision;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Double getOtherServices() {
		return otherServices;
	}

	public void setOtherServices(Double otherServices) {
		this.otherServices = otherServices;
	}

	public Double getLaborPrice() {
		return laborPrice;
	}

	public void setLaborPrice(Double laborPrice) {
		this.laborPrice = laborPrice;
	}

	public Double getRawMaterialPrice() {
		return rawMaterialPrice;
	}

	public void setRawMaterialPrice(Double rawMaterialPrice) {
		this.rawMaterialPrice = rawMaterialPrice;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getFreightType() {
		return freightType;
	}

	public void setFreightType(String freightType) {
		this.freightType = freightType;
	}

	public Date getDeliveryDeadline() {
		return deliveryDeadline;
	}

	public void setDeliveryDeadline(Date deliveryDeadline) {
		this.deliveryDeadline = deliveryDeadline;
	}

	public Date getPaymentDeadline() {
		return paymentDeadline;
	}

	public void setPaymentDeadline(Date paymentDeadline) {
		this.paymentDeadline = paymentDeadline;
	}

	public Tax getTax() {
		return tax;
	}

	public void setTax(Tax tax) {
		this.tax = tax;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public List<BudgetItems> getBudgetItems() {
		return budgetItems;
	}

	public void setBudgetItems(List<BudgetItems> budgetItems) {
		this.budgetItems = budgetItems;
	}
	
}
