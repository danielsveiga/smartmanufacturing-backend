package com.br.dsv.smartmanufacturing.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("personindividual")
public class PersonIndividual extends Person implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8716083789033528505L;

	private String cpf;
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	private String rg;
	
	private String surname;

}
