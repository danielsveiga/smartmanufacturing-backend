package com.br.dsv.smartmanufacturing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.br.dsv.smartmanufacturing.model.Person;

@SuppressWarnings("unchecked")
public interface PersonRepository extends JpaRepository<Person, Long>{
	
	public Person save(Person person);
	
	public Page<Person> findAll(Pageable pageResquest);
	
	public Person findById(Long id);

	@Query("from person p where p.isClient=True")
	public Page<Person> findByIsClient(Pageable pageResquest);
	
	@Query("from person p where p.isProvider=True")
	public Page<Person> findByIsProvider(Pageable pageResquest);
}
