package com.br.dsv.smartmanufacturing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.br.dsv.smartmanufacturing.model.Adress;

@SuppressWarnings("unchecked")
public interface AdressRepository extends JpaRepository<Adress, Long>{
	
	public Adress save(Adress adress);
	
	public Page<Adress> findAll(Pageable pageResquest);
	
	public Adress findById(Long id);

}
