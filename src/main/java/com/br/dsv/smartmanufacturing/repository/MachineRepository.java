package com.br.dsv.smartmanufacturing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.dsv.smartmanufacturing.model.Machine;

public interface MachineRepository extends JpaRepository<Machine, Long>{
	
	public Machine findById(Long id);

}
