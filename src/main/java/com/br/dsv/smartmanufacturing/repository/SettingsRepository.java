package com.br.dsv.smartmanufacturing.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.br.dsv.smartmanufacturing.model.Settings;

@SuppressWarnings("unchecked")
public interface SettingsRepository extends JpaRepository<Settings, Long>{

	public Settings save(Settings settings);
	
	public Page<Settings> findAll(Pageable pageResquest);
	
	public Settings findById(Long id);
	
}
